﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentKeeping.Services;
using StudentKeeping.Presentation.Students;

namespace StudentKeeping.Presentation
{
    static class Program
    {
        /// <summary>s
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InitialService.InitialDatabase();
            Application.Run(new SaveStudent(3));
        }
    }
}
