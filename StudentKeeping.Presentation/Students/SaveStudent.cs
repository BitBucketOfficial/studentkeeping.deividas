﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentKeeping.Dal.Models;
using StudentKeeping.Services;

namespace StudentKeeping.Presentation.Students
{
    public partial class SaveStudent : Form
    {
        private readonly StudentsService studentsService;
        public SaveStudent(int? id = null)
        {
            InitializeComponent();
            studentsService = new StudentsService();

            if (id != null)
            {


                var studentas = studentsService.GaukStudentąPagalId(id.Value);
                txtStudentoId.Text = studentas.Id.ToString();
                txtVardas.Text = studentas.Vardas;
                txtPavardė.Text = studentas.Pavardė;
                txtTelNr.Text = studentas.TelNr;
                txtAdresas.Text = studentas.Adresas;

            }
            else
            {
                MessageBox.Show("Toks ID jau egzistuoja");
            }
            
        }

        private void btnSaugoti_Click(object sender, EventArgs e)
        {
            var studentas = new Studentas()
            {
                Id = !string.IsNullOrWhiteSpace(txtStudentoId.Text) ? int.Parse(txtStudentoId.Text) : (int?)null,
                Vardas = txtVardas.Text,
                Pavardė = txtPavardė.Text,
                TelNr = txtTelNr.Text,
                Adresas = txtAdresas.Text
            };
            if (studentas.Id.HasValue)
            {
                studentsService.Redaguok(studentas);
            }
            else
            {
                studentsService.SukurkNauja(studentas);
            }
        }
    }
}
