﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using StudentKeeping.Dal;
using StudentKeeping.Dal.Models;

namespace StudentKeeping.Services
{
    public class StudentsService : DBConnection
    {
        public StudentsService()
            : base("StudentKeepingConnectionString")
        {}

        public Studentas GaukStudentąPagalId(int id)
        {
            Studentas studentas = null;
            var gaukStudentąPagalIdSql = "SELECT * FROM Studentai WHERE Id = @Id";


            try
            {
                var gaukStudentąPagalIdCommand = new MySqlCommand(gaukStudentąPagalIdSql, Connection);
                gaukStudentąPagalIdCommand.Parameters.AddWithValue("@Id", id);

                Connection.Open();
                var studentoExecuteReader = gaukStudentąPagalIdCommand.ExecuteReader();

                while (studentoExecuteReader.Read())
                {
                    studentas = new Studentas();
                    studentas.Id = studentoExecuteReader.GetInt32(0);
                    studentas.Vardas = studentoExecuteReader.GetString(1);
                    studentas.Pavardė = studentoExecuteReader.GetString(2);
                    studentas.TelNr = studentoExecuteReader.GetString(3);
                    studentas.Adresas = studentoExecuteReader.GetString(4);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Connection.Close();
            }


            return studentas;
        }

        public void SukurkNauja(Studentas studentas)
        {
            var sukurkNaujaStudentaSql = @"insert into Studentai (Vardas, Pavarde, TelNr, Adresas) 
                values (@vardas, @pavarde, @telnr, @adresas)";

            var sukurkNaujaStudentaCommand = new MySqlCommand(sukurkNaujaStudentaSql, Connection);

            sukurkNaujaStudentaCommand.Parameters.AddWithValue("@vardas", studentas.Vardas);
            sukurkNaujaStudentaCommand.Parameters.AddWithValue("@vardas", studentas.Pavardė);
            sukurkNaujaStudentaCommand.Parameters.AddWithValue("@vardas", studentas.TelNr);
            sukurkNaujaStudentaCommand.Parameters.AddWithValue("@vardas", studentas.Adresas);

            try
            {
                Connection.Open();
                sukurkNaujaStudentaCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                Connection.Close();

            }
        }
        public void Redaguok(Studentas studentas)
        {
            var redaguokStudentaSql = @"update Studentai 
                                       set vardas = @vardas, pavarde = @pavarde, TelNr = @telnr, Adresas = @adresas 
                                       where Id = @Id;";

            var redaguokStudentaCommand = new MySqlCommand(redaguokStudentaSql, Connection);

            redaguokStudentaCommand.Parameters.AddWithValue("@vardas", studentas.Vardas);
            redaguokStudentaCommand.Parameters.AddWithValue("@pavarde", studentas.Pavardė);
            redaguokStudentaCommand.Parameters.AddWithValue("@telnr", studentas.TelNr);
            redaguokStudentaCommand.Parameters.AddWithValue("@adresas", studentas.Adresas);
            redaguokStudentaCommand.Parameters.AddWithValue("@id", studentas.Id);

            try
            {
                Connection.Open();
                redaguokStudentaCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                Connection.Close();

            }
        }

    }
}
